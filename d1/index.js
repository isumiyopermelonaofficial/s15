console.log("Hello World!");

// Assignment Operator (=)

let assignmentNumber = 8;

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

assignmentNumber += 2;
console.log(assignmentNumber); //12

// Subtraction/Multiplication/Division Assignment Operator(-=, *=, /=)
assignmentNumber -=2;
assignmentNumber *=2;
assignmentNumber /=2;

// Arithmetic Operators(+, -, *, /, %)
// PEMDAS Rule

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement
let z = 1;
// Pre-Fix Incrementation
++z;
console.log(z);

// Post-Fix Incrementation
z++;
console.log(z);

console.log(z++);
console.log(z);

console.log(++z);

// Pre-fix Decrementation and Post-fix Decrementation
console.log(--z);
console.log(z--);
console.log(z);

// Type Coercion
// is the automatic or implicit conversion of values from one data type to another

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);
// Adding/Concatenating a string and a number will result to a string


let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
// The result is a number

let numE = true + 1;
console.log(numE);
console.log(typeof numE);
// the result is a number since boolean + number will be a number

let numF = false + 1;
console.log(numF);

// Comparison Operators
// (==) Equality Operator
let juan = 'juan';

console.log("Equality Operator");
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'JUAN'); //false - case sensitive
console.log('juan' == juan); //true

// (===)Strict Equality Operator
console.log("Strict Equality Operator");
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false - data type 
console.log(0 === false); //false - number and boolean
console.log('juan' === 'JUAN'); //false - case sensitive
console.log('juan' === juan); //true


//(!=)Inequality Operator
console.log("Inequality Operator");
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'JUAN'); //true
console.log('juan' != juan); //false

//(!==)Strict Inequality Operator
console.log("Strict Inequality Operator");
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN'); //true
console.log('juan' !== juan); //false

// Relational Comparison Operator
let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";

// Greater Than (>)
console.log("Greater Than: ");
console.log(x > y); //false

// Less Than (<)
console.log("Less Than: ");
console.log(y < y); //true
console.log(numString < 6000); //true - forced/type coercion to change string to a number
console.log(numString < 1000); //false

// Greater than or Equal to
console.log("Greater Than or Equal to: ");
console.log(w >= w); //true

// Less than or Equal to
console.log("Less Than or Equal to: ");
console.log(y <= y); //true

// Logical Operators (&&, ||, !)

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

// Logical ANN Operator (&& - Double Ampersand)
console.log("Logical AND Operator");
// return true if ALL operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //falce

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 95 && isAdmin;
console.log(authorization3);

// Logical OR Operator (|| - Double Pipe)
// return true if atleast ONE of the operand true
console.log("OR Operator");

let userLevel = 100;
let userLevel2 = 65;
let userAge = 15;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement1); //true

// NOT Operator(!)
console.log("NOT Operator");
// turns a boolean into the oppsite value

let opposite = !isAdmin;
console.log(opposite); //true - isAdmin original value is false
console.log(!isRegistered);

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true


// CONDITIONAL STATEMENT

//IF ELSE and IF Statement

// IF Statement
// if a specified condition is true
if(true) {
	console.log("We just run an if condition");
}

let numG = 5;
if(numG < 10) {
	console.log("Hello");
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length >= 10 && isRegistered && isAdmin) {
	console.log("Welcome to Game Online");
} else{
	console.log("You are not READY!");
}
//.lenght is a property of strings which determine the number of characters in string.

// Else statement
// execute a statement if all other conditions are false

if (userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge){
	console.log("Thank you for Joining the Noobies Guild");
} else {
	console.log("You are too strong to be a Noob");
}

// Else if statement
// executes a statement if previous conditions are false

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Welcome Noob");
} else if(userLevel3 > 25) {
	console.log("You are too strong");
} else if(userAge3 < requiredAge){
	console.log("You are too young to join the guild");
} else if(userName3.length < 10) {
	console.log("Username too short");
} else {
	console.log("You are not READY");
}


// If, else if and else statement functions

let n = 3;
console.log(typeof n);


function addNum(num1, num2){
	// Check if the numbers being passed as an arguments are number types
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if bnoth arguments passed are number types");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers");
	}
};

addNum(5,"k");

// Create a login function
function login(username, password){
	if(typeof username === "string" && typeof password === "string")
	{
		console.log("Both Arguments are string");
		// Nested if-else
		if(username.length >= 8 && password.length >= 8){
			console.log("Thank you for Logging In");
		} else if(username.length < 8) {
			console.log("Username is too short");
		} else if(password.length < 8) {
			console.log("Password is too short");
		} else {
			console.log("Credentials are too short");
		}
		// End Nested if-else
	}
	else {
		console.log("One or both of the arguments are not a string");
	}
}

login("Isu", "pass");

// function with return keyword
let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30){
		return 'Not a typhoon yet';
	} else if(windSpeed <= 61) {
		return 'Tropical depression detected.';
	} else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical Storm Detected';
	} else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.';
	} else {
		return 'Typhone Detected';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);

if(message == 'Tropical Storm Detected'){
	console.warn(message);
}
// console.warn is a good way yo print warnings in our console that could help us developers act on a certain output within our code

// Truthy and Falsy
if([]) {
	console.log("Truthy");
}

let fName = "Jane";
let mName = "Doe";
let lName = "Smith";

console.log(fName + " " + mName + " " + lName);
// Template literals (ES6)
console.log(`${fName} ${mName} ${lName} `);

// Ternary Operator (ES6)
/*
	Syntax:
		(expression/condition) ? ifTrue : ifFalse;
		(expression/condition) ? ifTrue : ifFalse;
*/
// Single Statement execution
let result = (1 < 18) ? true:false;
console.log(result);

5000 > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000");


// Multiple Statement Execution
let name;
function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit'
}

function isUnderAge() {
	name = 'Jane';
	return 'You are under the age limit'
}

// let age = parseInt(prompt("What is your age?"));
// console.log(age);

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log(`Result of the ternary operator in function: ${legalAge}, ${name}`)

// Switch Statement
/*
	- Can be used as an alternative to an if else statment
	- Syntax:

		switch(expression){
			case value1:
				statement;
				break;
			case value2:
				statement;
				break;
			.
			.
			.
			case valueN:
				statement;
				break
			default:
				statement;
		}
*/

// let day = prompt("What day of the week is it today: ").toLowerCase();
// console.log(day);

// switch(day){
// 	case 'monday':
// 		  console.log("The color of the day is red");
// 		  break;
// 	case 'tuesday':
// 		  console.log("The color of the day is orange");
// 		  break;
// 	case 'wednesday':
// 		  console.log("The color of the day is yellow");
// 		  break;
// 	case 'thursday':
// 		  console.log("The color of the day is green");
// 		  break;
// 	case 'friday':
// 		  console.log("The color of the day is blue");
// 		  break;
// 	case 'saturday':
// 		  console.log("The color of the day is indigo");
// 		  break;
// 	case 'sunday':
// 		  console.log("The color of the day is violet");
// 		  break;
// 	default:
// 		  console.log("Please Input a valid day");
	
// }

let v = prompt("Enter wind speed:");

switch (true) {
    case (v <25):
        alert("not a strong wind");
        break;
    case (v >= 25 && v < 39):
        alert("strong wind");
        break;
    case (v >= 39 && v < 55):
        alert("gale");
        break;
     case (v >= 55 && v < 73):
        alert("whole gale");
        break;
    default:
        alert("hurricane");
        break;
}

// Try-Catch-Finally Statement

function showIntensityAlert(windSpeed){
	try{
		// Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(typeof error);
		console.log(error.message);
	}
	finally{
		// Continue to execute code regardless of success or failure of code exucution in the try block
		alert('Intensity updates whill show new alert');
	}
}

showIntensityAlert(56);
	