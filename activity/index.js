// Test Connection
console.log("Hello World");

// Prompt 2 numbers
let num1 = parseInt(prompt("Enter the First Number: "));
let num2 = parseInt(prompt("Enter the Second Number: "));

let sum = num1 + num2;


//Start Basic Arithmetic
if(sum < 10){
	console.warn("The Sum of " + num1 + " and " + num2 + " is: " + sum);
}else if(sum >= 10 && sum <= 20){
	let diff = num1 - num2;
	alert("The Difference of " + num1 + " and " + num2 + " is: " + diff);	
}else if(sum >= 21 && sum <= 30){
	let prod = num1 * num2;
	alert("The Product of " + num1 + " and " + num2 + " is: " + prod);
}else if(sum >= 31){
	let quot = num1 / num2;
	alert("The Quotient of " + num1 + " and " + num2 + " is: " + quot);
}
// End Basic Arithmetic

// Prompt Name and Age
let userName = prompt("What is your Name?");
let userAge = parseInt(prompt("What is your Age?"));

if(!userName || !userAge){
	alert("Are you a Time Traveler?");
}else{
	alert("Hello " + userName + " you are " + userAge + " years old");
}
// End of Name and Age Prompt

function isLegalAge(userAge){
	if(userAge >= 18){
		alert("You are of Legal Age");
	}else if(userAge < 18){
		alert("You are not allowed here");
	}
}

isLegalAge(userAge);
//console.log(userAge);
switch(true){
	case(userAge == 18):
		alert("You are now allowed to party");
		break;
	case(userAge == 21):
		alert("You are now part of the adult society");
		break;
	case(userAge == 65):
		alert("We thank you for your contribution to society");
		break;
	default:
		alert("Are you sure you're not an alien");
		break;
}

function sampleTryCatch(Age){
	try{
		alerta(isLegalAge(Age));
	}
	catch(error){
		console.warn(typeof error);
		console.warn(error.message);
	}
	finally{
		isLegalAge(Age);
	}

}

sampleTryCatch(32)